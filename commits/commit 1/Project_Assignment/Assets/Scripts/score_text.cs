﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class score_text : MonoBehaviour {

	Text Score;
	public static int coinAmount = 0;

	void Start () {
		Score = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
		Score.text = "Score: " + coinAmount.ToString ();
		
	}
}
