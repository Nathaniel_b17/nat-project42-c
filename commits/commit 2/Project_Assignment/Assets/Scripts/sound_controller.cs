﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sound_controller : MonoBehaviour {

	public Slider volume;
	public AudioSource BackgroundMusic;

	public static sound_controller instance = null;

	// Update is called once per frame
	void Update () {
		if (volume != null) {
			BackgroundMusic.volume = volume.value;
		}
	}

	void Awake()  {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (this.gameObject);
		DontDestroyOnLoad (gameObject);
	}
}
