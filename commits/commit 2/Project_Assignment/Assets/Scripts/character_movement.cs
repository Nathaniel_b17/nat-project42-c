﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class character_movement : MonoBehaviour {

    //Declaring a variable that can only be accessed on this script
    private Rigidbody2D char_RB;
	public string midJump = "n";
	public float speed;
	public float jump;
	float moveVelocity;

    void Start()
    {

        //Getting the rigidbody 2d component and storing it into variable
        char_RB = this.GetComponent<Rigidbody2D>();

    }

    void Update()
    {

        //When user presses W
		if (Input.GetKey(KeyCode.Space) && (midJump == "n"))
        {
			char_RB.velocity = new Vector2 (char_RB.velocity.x, jump);
			midJump = "y";
        }
        else if (Input.GetKey(KeyCode.S)) //When user presses S
        {
            char_RB.velocity = new Vector2(0f, -5f); //Move down by 5
        }
        else if (Input.GetKey(KeyCode.A)) //When user presses A
        {
            char_RB.velocity = new Vector2(-5f, 0f); //Move left by 5
        }
        else if (Input.GetKey(KeyCode.D)) //When user presses D
        {
            char_RB.velocity = new Vector2(5f, 0f); //Move right by 5
        }
        else //If user presses nothing 
        {
            char_RB.velocity = new Vector2(0f, 0f); //Don't move
        }
       
    }

	void OnCollisionEnter2D(Collision2D collision)
	{
		Vector3 normal = collision.contacts [0].normal;
		if (normal.y > 0) {
			midJump = "n";
		}
	}
}


