﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class parallax_Controller : MonoBehaviour
{

    public bool linkedToCamera, looping;

    public Vector3 speed = new Vector3();

    public Vector3 direction = new Vector3();

    public List<SpriteRenderer> backgroundImages;

    // Use this for initialization
    void Start()
    {

        if (looping)
        {
            backgroundImages = new List<SpriteRenderer>();
            foreach (Transform t in this.transform)
            {
                if (t.GetComponent<SpriteRenderer>() != null)
                {
                    backgroundImages.Add(t.GetComponent<SpriteRenderer>());
                }
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

        Vector3 movement = new Vector3(speed.x * direction.x, speed.y * direction.y);

        movement = movement * Time.deltaTime;

        transform.Translate(movement);

        if (linkedToCamera)
        {
            Camera.main.transform.Translate(movement);
        }

        if (looping)
        {
            SpriteRenderer firstImage = backgroundImages.FirstOrDefault();

            if (firstImage != null)
            {

                if (firstImage.IsVisibleFrom(Camera.main) == false)
                {

                    SpriteRenderer lastImage = backgroundImages.LastOrDefault();

                    Vector3 lastImagePosition = lastImage.transform.position;

                    Vector3 lastImageSize = (lastImage.bounds.max - lastImage.bounds.min);

                    //place the first image after the last image
                    firstImage.transform.position =
                        new Vector3(lastImagePosition.x + lastImageSize.x,
                            firstImage.transform.position.y);

                    backgroundImages.Remove(firstImage);

                    backgroundImages.Add(firstImage);

                }
            }


        }


    }
}
