﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coin_spawner : MonoBehaviour {

	public GameObject coins;
	float randY;
	float randX;
	Vector2 whereToSpawn;
	public float spawnRate = 2f;
	float nextSpawn = 0;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		if (Time.time > nextSpawn) 
		{
			nextSpawn = Time.time + spawnRate;
			randY = Random.Range (-2.4f, 0f);
			randX = Random.Range (0f, 5.8f);

			whereToSpawn = new Vector2 (randX, randY);
			Instantiate(coins, whereToSpawn, Quaternion.identity);

		}

	}
}