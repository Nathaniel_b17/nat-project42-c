﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class game_Controller : MonoBehaviour {

	public bool paused;

    // Use this for initialization
    void Start()
	{
		paused = false;
    }

	void Update()
	{
		if (Input.GetKey (KeyCode.P)) 
		{
			paused = !paused;
		}
		if (paused) {
			
			Time.timeScale = 0;

		} else if (!paused) 
		{
			Time.timeScale = 1;
		}

	}
}
