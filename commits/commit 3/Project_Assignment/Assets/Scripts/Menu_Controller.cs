﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu_Controller : MonoBehaviour
{

    public void LoadTheLevel(string LevelName) //Function called LoadTheLevel
    {
        SceneManager.LoadScene(LevelName);
    }

    public void Quit_Game() //Function called Quit_Game
    {
		UnityEditor.EditorApplication.isPlaying = false; 
	}

	public void RestartGame() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name); // loads current scene
	}

    public void LoadTheNextLevel() //Function called LoadTheNextLevel
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); //Load the next level by incrementing
    }


}
