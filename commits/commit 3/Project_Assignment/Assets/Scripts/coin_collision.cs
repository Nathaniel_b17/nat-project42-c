﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coin_collision : MonoBehaviour {

	public GameObject coinToDestroy;
	private AudioSource audioSource;
	public AudioClip coinSound;

	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.gameObject.tag.Equals ("character")) 
		{
			score_text.score += 10;
			Destroy (gameObject); 
			audioSource = GetComponent<AudioSource>();
			audioSource.clip = coinSound;
			audioSource.Play();
		}

	}
}