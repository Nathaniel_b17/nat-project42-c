﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet_collision : MonoBehaviour {

	public GameObject bulletToDestroy;
	private AudioSource audioSource;
	public AudioClip bulletSound;

	void OnTriggerEnter2D(Collider2D other)
	{

		if (other.gameObject.tag.Equals ("character")) 
		{
			score_text.score -= 10;
			Destroy (gameObject); 
			audioSource = GetComponent<AudioSource>();
			audioSource.clip = bulletSound;
			audioSource.Play();
		}

	}
}