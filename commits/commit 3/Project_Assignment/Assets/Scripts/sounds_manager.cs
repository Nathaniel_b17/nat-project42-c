﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sounds_manager : MonoBehaviour {

	public GameObject sButton;
	public Sprite audioOn;
	public Sprite audioOff;


	// Use this for initialization
	void Start () {
		if (AudioListener.pause == true) {
			sButton.GetComponent<Image> ().sprite = audioOff;
		} else 
		{
			sButton.GetComponent<Image> ().sprite = audioOn;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (AudioListener.pause == true) {
			AudioListener.pause = false;
			sButton.GetComponent<Image> ().sprite = audioOn;
		} else {
			AudioListener.pause = true;
			sButton.GetComponent<Image> ().sprite = audioOff;
		}
	}
}
